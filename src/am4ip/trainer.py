
import torch
import numpy as np
from typing import Callable
import torch.utils.data as data


class BaselineTrainer:
    def __init__(self, model: torch.nn.Module,
                 loss: Callable,
                 optimizer: torch.optim.Optimizer,
                 use_cuda=True):
        self.loss = loss
        self.use_cuda = use_cuda
        self.optimizer = optimizer

        self.device = "cuda:0" if use_cuda else "cpu"
        self.model = model.to(self.device)

    def fit(self, train_data_loader: data.DataLoader,
            epoch: int):
        n_digit = int(np.log10(len(train_data_loader))) + 1
        avg_loss = 0.
        self.model.training = True
        for e in range(epoch):
            print(f"Start epoch {e+1}/{epoch}")
            n_batch = 0
            for i, (dist_img, ref_img) in enumerate(train_data_loader):
                # Reset previous gradients
                self.optimizer.zero_grad()

                # Move data to cuda is necessary:
                if self.use_cuda:
                    ref_img = ref_img.to(self.device)
                    dist_img = dist_img.to(self.device)

                # Make forward
                # TODO change this part to fit your loss function
                loss = self.loss(self.model.forward(dist_img), ref_img)
                loss.backward()

                # Adjust learning weights
                self.optimizer.step()
                avg_loss += loss.items()
                n_batch += 1

                print(f"\r{i+1:0{n_digit}d}/{len(train_data_loader)}: loss = {avg_loss / n_batch}", end='')
            print()

        return avg_loss
